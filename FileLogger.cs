﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Jitesoft.Log
{
    /// <summary>
    /// Basic file writer using a lock to keep the file syncronized and threadsafe.
    /// </summary>
    public class FileLogger : Logger
    {
        private readonly object defaultLock = new object();
        private readonly string fileName;
        private readonly IDictionary<string, string> fileNames;

        /// <summary>
        /// Timestamp format. Defaults to "O" which is the ISO 8601 standard output.
        /// </summary>
        public string TimestampFormat { get; set; } = "O";

        /// <summary>
        /// Initialize a new filelogger.
        /// </summary>
        /// <param name="fileName">File name to print all logs to.</param>
        public FileLogger(string fileName)
        {
            this.fileName = fileName;
            this.fileNames = null;
        }

        /// <summary>
        /// Initialize a new filelogger.
        /// </summary>
        /// <param name="fileNames">A dictionary of tags and filenames where the tag is key and filename value.</param>
        public FileLogger(IDictionary<string, string> fileNames)
        {
            this.fileName = null;
            this.fileNames = fileNames;
        }

        /// <summary>
        /// General logging method.
        /// Tag is intended to be used as a custom user-specified string.
        /// </summary>
        /// <param name="tag">Custom tag.</param>
        /// <param name="message">Message to log.</param>
        public override void Log(string tag, string message)
        {
            lock (defaultLock)
            {
                string logFile = this.fileName ?? fileNames[tag];
                File.AppendAllText(logFile, $"{tag} ({DateTime.Now:TimestampFormat}): {message}{Environment.NewLine}");
            }
        }
    }
}
