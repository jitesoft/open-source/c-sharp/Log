﻿namespace Jitesoft.Log
{
    /// <summary>
    /// Logger interface.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// General logging method.
        /// Tag is intended to be used as a custom user-specified string.
        /// </summary>
        /// <param name="tag">Custom tag.</param>
        /// <param name="message">Message to log.</param>
        void Log(string tag, string message);

        /// <summary>
        /// General loging method.
        /// </summary>
        /// <param name="tag">Tag as a LogTag enum value.</param>
        /// <param name="message">Message to log.</param>
        void Log(LogTag tag, string message);

        /// <summary>
        /// Write a message to the debug log.
        /// <example>
        /// Debug information.
        /// </example>
        /// </summary>
        /// <param name="message">Message to log.</param>
        void Debug(string message);

        /// <summary>
        /// Write a message to the info log.
        /// </summary>
        /// <example>
        /// Interesting events.
        /// </example>
        /// <param name="message"></param>
        void Info(string message);

        /// <summary>
        /// Write a message to the notice log.
        /// </summary>
        /// <example>
        /// Normal events.
        /// </example>
        /// <param name="message">Message to log.</param>
        void Notice(string message);

        /// <summary>
        /// Write a message to the warning log.
        /// </summary>
        /// <example>
        /// Information that should warn the user alt developer.
        /// Such as deprecation warnings.
        /// </example>
        /// <param name="message">Message to log.</param>
        void Warning(string message);

        /// <summary>
        /// Write a message to the error log.
        /// </summary>
        /// <example>
        /// Runtime errors that need to be fixed.
        /// </example>
        /// <param name="message">Message to log.</param>
        void Error(string message);

        /// <summary>
        /// Write a message to the critical log.
        /// </summary>
        /// <example>
        /// Critical conditions.
        /// Uncaught exceptions and similar.
        /// </example>
        /// <param name="message">Message to log.</param>
        void Critical(string message);

        /// <summary>
        /// Write a message to the alert log.
        /// </summary>
        /// <example>
        /// Entire application or page down, server is burning.
        /// </example>
        /// <param name="message">Message to log.</param>
        void Alert(string message);

        /// <summary>
        /// Write a message to the emergency log.
        /// </summary>
        /// <example>
        /// System unusable.
        /// </example>
        /// <param name="message">Message to log.</param>
        void Emergency(string message);
    }
}
