﻿using System;
using System.IO;
using System.Reflection;
using Jitesoft.Log;
using Xunit;

namespace Log.Tests
{
    public class StdLoggerTest
    {
        [Fact]
        public void TestLogWithUserTag()
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);
                Console.SetError(writer);
                StdLogger logger = new StdLogger()
                {
                    TimestampFormat = "Y"
                };

                logger.Log("ABC123", "A Message");
                Assert.Equal($"ABC123 ({DateTime.Now:Y}): A Message{Environment.NewLine}", writer.ToString());
            }
        }
        
        [Theory]
        [InlineData(LogTag.Debug)]
        [InlineData(LogTag.Notice)]
        [InlineData(LogTag.Info)]
        [InlineData(LogTag.Warning)]
        [InlineData(LogTag.Error)]
        [InlineData(LogTag.Alert)]
        [InlineData(LogTag.Critical)]
        [InlineData(LogTag.Emergency)]
        public void TestLogWithTag(LogTag tag)
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);
                Console.SetError(writer);
                StdLogger logger = new StdLogger()
                {
                    TimestampFormat = "Y"
                };

                logger.Log(tag, "A Message");
                Assert.Equal($"{tag} ({DateTime.Now:Y}): A Message{Environment.NewLine}", writer.ToString());
            }
        }


        [Theory]
        [InlineData(LogTag.Debug)]
        [InlineData(LogTag.Notice)]
        [InlineData(LogTag.Info)]
        [InlineData(LogTag.Warning)]
        [InlineData(LogTag.Error)]
        [InlineData(LogTag.Alert)]
        [InlineData(LogTag.Critical)]
        [InlineData(LogTag.Emergency)]
        public void TestCallTagMethod(LogTag tag)
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);
                Console.SetError(writer);
                StdLogger logger = new StdLogger()
                {
                    TimestampFormat = "Y"
                };

                Type thisType = logger.GetType();
                MethodInfo theMethod = thisType.GetMethod(tag.ToString());
                theMethod.Invoke(logger, new[] {"A Message"});

                Assert.Equal($"{tag} ({DateTime.Now:Y}): A Message{Environment.NewLine}", writer.ToString());
            }

        }
    }
}
