﻿namespace Jitesoft.Log
{
    /// <summary>
    /// Pre-defined logging tags.
    /// </summary>
    public enum LogTag
    {
        Debug,
        Info,
        Notice,
        Warning,
        Error,
        Critical,
        Alert,
        Emergency
    }
}
