﻿using System;

namespace Jitesoft.Log
{
    /// <summary>
    /// Baseclass with followthrough to the abstract Log method.
    /// </summary>
    public abstract class Logger : ILogger
    {
        /// <summary>
        /// General logging method.
        /// Tag is intended to be used as a custom user-specified string.
        /// </summary>
        /// <param name="tag">Custom tag.</param>
        /// <param name="message">Message to log.</param>
        public abstract void Log(string tag, string message);

        /// <summary>
        /// General loging method.
        /// </summary>
        /// <param name="tag">Tag as a LogTag enum value.</param>
        /// <param name="message">Message to log.</param>
        public void Log(LogTag tag, string message)
        {
            this.Log(Enum.GetName(typeof(LogTag), tag), message);
        }

        /// <summary>
        /// Write a message to the debug log.
        /// <example>
        /// Debug information.
        /// </example>
        /// </summary>
        /// <param name="message">Message to log.</param>
        public void Debug(string message)
        {
            this.Log(LogTag.Debug, message);
        }

        /// <summary>
        /// Write a message to the info log.
        /// </summary>
        /// <example>
        /// Interesting events.
        /// </example>
        /// <param name="message"></param>
        public void Info(string message)
        {
            this.Log(LogTag.Info, message);
        }

        /// <summary>
        /// Write a message to the notice log.
        /// </summary>
        /// <example>
        /// Normal events.
        /// </example>
        /// <param name="message">Message to log.</param>
        public void Notice(string message)
        {
            this.Log(LogTag.Notice, message);
        }

        /// <summary>
        /// Write a message to the warning log.
        /// </summary>
        /// <example>
        /// Information that should warn the user alt developer.
        /// Such as deprecation warnings.
        /// </example>
        /// <param name="message">Message to log.</param>
        public void Warning(string message)
        {
            this.Log(LogTag.Warning, message);
        }

        /// <summary>
        /// Write a message to the error log.
        /// </summary>
        /// <example>
        /// Runtime errors that need to be fixed.
        /// </example>
        /// <param name="message">Message to log.</param>
        public void Error(string message)
        {
            this.Log(LogTag.Error, message);
        }

        /// <summary>
        /// Write a message to the critical log.
        /// </summary>
        /// <example>
        /// Critical conditions.
        /// Uncaught exceptions and similar.
        /// </example>
        /// <param name="message">Message to log.</param>
        public void Critical(string message)
        {
            this.Log(LogTag.Critical, message);
        }

        /// <summary>
        /// Write a message to the alert log.
        /// </summary>
        /// <example>
        /// Entire application or page down, server is burning.
        /// </example>
        /// <param name="message">Message to log.</param>
        public void Alert(string message)
        {
            this.Log(LogTag.Alert, message);
        }

        /// <summary>
        /// Write a message to the emergency log.
        /// </summary>
        /// <example>
        /// System unusable.
        /// </example>
        /// <param name="message">Message to log.</param>
        public void Emergency(string message)
        {
            this.Log(LogTag.Emergency, message);
        }
    }
}
