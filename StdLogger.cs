﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Jitesoft.Log
{
    public class StdLogger : Logger
    {
        private readonly object defaultLock = new object();

        /// <summary>
        /// Timestamp format. Defaults to "O" which is the ISO 8601 standard output.
        /// </summary>
        public string TimestampFormat { get; set; } = "O";
        
        /// <summary>
        /// General logging method.
        /// Tag is intended to be used as a custom user-specified string.
        /// </summary>
        /// <param name="tag">Custom tag.</param>
        /// <param name="message">Message to log.</param>
        public override void Log(string tag, string message)
        {
            lock (defaultLock)
            {
                string msg = $"{tag} ({DateTime.Now.ToString(TimestampFormat)}): {message}{Environment.NewLine}";
                if (LogTag.TryParse(tag, true, out LogTag result) && result > LogTag.Warning)
                {
                    Console.Error.Write(msg);
                }
                else
                {
                    Console.Write(msg);
                }
            }
        }
    }
}
